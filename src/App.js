import { useRef } from "react";
import { useEffect } from "react";
import { useState } from "react";
import useScreenSize from "./useScreenSize";

const App = () => {
  const { screenSize } = useScreenSize();

  const [inProgress, setInProgress] = useState(false);
  const [secondLeft, setSecondLeft] = useState(0);
  const block_1 = useRef();
  const circle = useRef();

  const onStart = () => {
    setInProgress(true);
    setSecondLeft(5);
  };

  useEffect(() => {
    if (inProgress) {
      setTimeout(() => {
        setInProgress(false);
      }, 5000);

      const start = block_1?.current.getBoundingClientRect();
      circle.current.style.top = `${start.y + start.height / 2}px`;
      circle.current.style.left = `${75}px`;
      circle.current.style.display = "block";

      setTimeout(() => {
        circle.current.style.top = `${(screenSize.height - 25) / 2}px`;
        circle.current.style.left = `${screenSize.width - 125}px`;
      }, 0);
    }
  }, [inProgress, screenSize]);

  // useEffect(() => {
  //   console.log(circle.current?.getBoundingClientRect());
  //   if (circle.current?.getBoundingClientRect().x >= screenSize.width - 125) {
  //     // circle.current.style.display = "none";
  //   }
  // }, [circle?.current?.getBoundingClientRect(), screenSize]);
  console.log(circle);
  useEffect(() => {
    if (secondLeft !== 0) {
      const interval = setInterval(() => {
        setSecondLeft((prev) => --prev);
      }, 1000);

      return () => clearInterval(interval);
    }
  }, [secondLeft]);

  return (
    <div className="App">
      <div className="block_wrapper">
        <div className="block_1" ref={block_1}>
          1
        </div>
        <div className="block_2">2</div>
        <div
          className="circle"
          ref={circle}
          onTransitionEnd={() => {
            circle.current.style.display = "none";
          }}
        ></div>
      </div>
      <div className="button_wrapper">
        <button
          className="start_button"
          type="button"
          onClick={onStart}
          disabled={inProgress}
        >
          {!inProgress ? "START" : secondLeft}
        </button>
      </div>
    </div>
  );
};

export default App;
